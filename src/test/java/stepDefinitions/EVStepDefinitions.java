package stepDefinitions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import driver.SeleniumWebDriver;
import net.thucydides.core.annotations.Steps;
import pageObjects.EVpages;
import steps.EVSteps;

public class EVStepDefinitions {
    @Steps
    EVSteps steps = new EVSteps();

    @Given("^Necesito ingresar al sistema$")
    public void necesitoIngresarAlSistema() {
        SeleniumWebDriver.chrome("https://epxstandardcoretesting.azurewebsites.net/");
    }

    @When("^me encuentro en el login$")
    public void meEncuentroEnElLogin() {
        steps.busqueda();
    }

    @Then("^ingreso las credenciales y doy click en iniciar sesion$")
    public void ingresoLasCredencialesYDoyClick() {
        SeleniumWebDriver.driver.close();
        SeleniumWebDriver.driver.quit();
    }

}
