package stepDefinitions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import driver.SeleniumWebDriver;
import net.thucydides.core.annotations.Steps;
import steps.EVNuevoParticular;
import steps.EVSteps;

public class EVNuevoParticularStepDefinitions {
    @Steps
    EVSteps steps = new EVSteps();
    @Steps
    EVNuevoParticular stepNuevoParticular = new EVNuevoParticular();

    @Given("^Necesito ingresar al sistema y crear un nuevo particular$")
    public void necesitoIngresarAlSistemaYCrearUnParticular() {
        SeleniumWebDriver.chrome("https://epxstandardcoretesting.azurewebsites.net/");
    }

    @When("^estoy en el login$")
    public void meEncuentroEnElLogin() {
        steps.busqueda();
        stepNuevoParticular.navegar();
    }

    @Then("^ingreso las credenciales y doy click en iniciar sesion y creo un particular$")
    public void ingresoLasCredencialesYDoyClick() {
        SeleniumWebDriver.driver.close();
        SeleniumWebDriver.driver.quit();
    }

}
