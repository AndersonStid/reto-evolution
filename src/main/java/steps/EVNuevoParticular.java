package steps;

import net.thucydides.core.annotations.Step;
import pageObjects.EVLocalizadorNuevoParticular;
import pageObjects.EVpages;
import utils.Excel;

import java.io.IOException;

public class EVNuevoParticular extends EVStepBase{
    EVLocalizadorNuevoParticular localizadores = new EVLocalizadorNuevoParticular();
    @Step
    public void navegar(){
        try {
            leerExcel = Excel.lecturaExcel("NuevoParticular.xlsx","hojaProyecto");
        }catch (IOException e){
            e.printStackTrace();
        }

        darClick(localizadores.getClickPersonaliza());
        try {
            Thread.sleep(1000);
        }catch (InterruptedException e){
            e.printStackTrace();
        }
        darClick(localizadores.getBuscarparticular());
        try {
            Thread.sleep(600);
        }catch (InterruptedException e){
            e.printStackTrace();
        }
        darClick(localizadores.getAgregarparticular());
        try {
            Thread.sleep(600);
        }catch (InterruptedException e){
            e.printStackTrace();
        }
        darClick(localizadores.getSelectTipoPersona());
        try {
            Thread.sleep(600);
        }catch (InterruptedException e){
            e.printStackTrace();
        }
        darClick(localizadores.getSelectInputTipoPersona());
        try {
            Thread.sleep(600);
        }catch (InterruptedException e) {
            e.printStackTrace();
        }
        darClick(localizadores.getSelectTipodeDocumento());
        try {
            Thread.sleep(600);
        }catch (InterruptedException e){
            e.printStackTrace();
        }
        darClick(localizadores.getSelectOpcionTipoDocumento());
        try {
            Thread.sleep(1000);
        }catch (InterruptedException e){
            e.printStackTrace();
        }


        escribirTextos(localizadores.getSelectAgregarPrimerNombre(), leerExcel.get(1).get("Nombre Columna"));
        try {
            Thread.sleep(1000);
        }catch (InterruptedException e) {
            e.printStackTrace();
        }
        escribirTextos(localizadores.getSelectAgregarPrimerApellido(), leerExcel.get(2).get("Nombre Columna"));
        try {
            Thread.sleep(600);
        }catch (InterruptedException e) {
            e.printStackTrace();
        }
        escribirTextos(localizadores.getSelectAgregarDireccion(), leerExcel.get(3).get("Nombre Columna"));
        try {
            Thread.sleep(600);
        }catch (InterruptedException e) {
            e.printStackTrace();
        }
        escribirTextos(localizadores.getSelectAgregarPIdentificacion(), leerExcel.get(0).get("Nombre Columna"));
        try {
            Thread.sleep(1000);
        }catch (InterruptedException e) {
            e.printStackTrace();
        }
        scroll();

        try {
            Thread.sleep(600);
        }catch (InterruptedException e) {
            e.printStackTrace();
        }

        darClick(localizadores.getSelectAgregarOpcionTitulo());
        try {
            Thread.sleep(600);
        }catch (InterruptedException e) {
            e.printStackTrace();
        }
        darClick(localizadores.getSelectOpcionTipoTitulo());
        try {
            Thread.sleep(600);
        }catch (InterruptedException e) {
            e.printStackTrace();
        }

        darClick(localizadores.getSelectAgregarOpcionDepartamento());
        try {
            Thread.sleep(600);
        }catch (InterruptedException e) {
            e.printStackTrace();
        }

        darClick(localizadores.getSelectOpcionDepartamento());
        try {
            Thread.sleep(2000);
        }catch (InterruptedException e) {
            e.printStackTrace();
        }
        darClick(localizadores.getSelectagregarCiudad());
        try {
            Thread.sleep(600);
        }catch (InterruptedException e) {
            e.printStackTrace();
        }
        darClick(localizadores.getSelectOpcionCiudad());
        try {
            Thread.sleep(600);
        }catch (InterruptedException e) {
            e.printStackTrace();
        }
        escribirTextos(localizadores.getInputAgregarEstrato(), leerExcel.get(4).get("Nombre Columna"));
        try {
            Thread.sleep(1000);
        }catch (InterruptedException e) {
            e.printStackTrace();
        }

        darClick(localizadores.getSelectagregarGenero());
        try {
            Thread.sleep(1000);
        }catch (InterruptedException e) {
            e.printStackTrace();
        }
        darClick(localizadores.getSelectOpcionGenero());
        try {
            Thread.sleep(1000);
        }catch (InterruptedException e) {
            e.printStackTrace();
        }
        darClick(localizadores.getClickGuardar());
        try {
            Thread.sleep(10000);
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }
}
