package steps;

import driver.SeleniumWebDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import java.util.ArrayList;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class EVStepBase {

    public static ArrayList<Map<String, String>> leerExcel = new ArrayList<Map<String, String>>();

    public void escribirTextos(By elemento, String texto) {
        SeleniumWebDriver.driver.findElement(elemento).sendKeys(texto);
    }

    public void escribirTextosConEnter(By elemento, String texto) {
        SeleniumWebDriver.driver.findElement(elemento).sendKeys(texto, Keys.ENTER);
    }

    public void darClick(By elemento) {
        SeleniumWebDriver.driver.findElement(elemento).click();

    }

    public void scroll() {
        Actions act = new Actions(SeleniumWebDriver.driver);
        act.sendKeys(Keys.PAGE_DOWN).build().perform();

    }

    public String obtenerLbl(By elemento) {
        return SeleniumWebDriver.driver.findElement(elemento).getText();
    }

    public void validation(By elemento, String texto) {
        assertEquals(obtenerLbl(elemento), texto);
    }

    public void limpiarTexto(By elemento) {
        SeleniumWebDriver.driver.findElement(elemento).clear();
    }
}
