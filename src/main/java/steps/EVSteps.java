package steps;

import net.thucydides.core.annotations.Step;
import pageObjects.EVpages;
import utils.Excel;

import java.io.IOException;

public class EVSteps extends EVStepBase{
    EVpages localizadores = new EVpages();
    @Step
    public void busqueda(){
        try {
            leerExcel = Excel.lecturaExcel("Terminosbusqueda.xlsx","hojaProyecto");
        }catch (IOException e){
            e.printStackTrace();
        }
        darClick(localizadores.getIngresosuariotxt());
        try {
            Thread.sleep(60);
        }catch (InterruptedException e){
            e.printStackTrace();
        }
        escribirTextos(localizadores.getIngresosuariotxt(), leerExcel.get(0).get("Nombre Columna"));
        try {
            Thread.sleep(60);
        }catch (InterruptedException e) {
            e.printStackTrace();
        }
        darClick(localizadores.getIngresarcontrasena());
        try {
            Thread.sleep(60);
        }catch (InterruptedException e){
            e.printStackTrace();
        }
        escribirTextos(localizadores.getIngresarcontrasena(), leerExcel.get(1).get("Nombre Columna"));
        try {
            Thread.sleep(60);
        }catch (InterruptedException e){
            e.printStackTrace();
        }

        darClick(localizadores.getClickboton());
        try {
            Thread.sleep(600);
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }
}