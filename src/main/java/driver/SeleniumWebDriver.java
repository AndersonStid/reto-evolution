package driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class SeleniumWebDriver {
    //creacion del driver del navegador
    public  static WebDriver driver;
    //creacion de un metodo con el que se va a levantar el navegador
    public  static void chrome(String url){
        ChromeOptions options = new ChromeOptions();// creamos un objeto
        options.addArguments("--start-maximized");//llamado de argumento maximizar
        options.addArguments("--ignore-certificate-errors");//argumento  ignora los errores de certificado
        options.addArguments("--disable-infobars");//argumento

        driver = new ChromeDriver(options);
        driver.get(url);
    }
}
