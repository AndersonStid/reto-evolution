package pageObjects;

import org.openqa.selenium.By;

public class EVLocalizadorNuevoParticular {

    private By clickPersonaliza = By.xpath("//a[@id='foo_lbOpcion_13']");

    private  By buscarparticular = By.xpath("//a[@id='foo_bar_13_lbOpcion_1']");

    private  By agregarparticular = By.xpath("//a[@id='MainContent_btnNuevoContacto']");

    private  By selectTipoPersona = By.xpath("//div[@id='TipoPersona_chosen']");
    private  By selectInputTipoPersona = By.xpath("//div/div/div/div/ul/li[1]");
    private  By selectTipodeDocumento = By.xpath("//div[@id='TipoIdentificacion_chosen']");
    private  By selectOpcionTipoDocumento = By.xpath("//div[2]/div/div/div/ul/li[2]");

    private  By SelectAgregarPIdentificacion = By.xpath("//input[@id='txtIdentificacion']");
    private  By SelectAgregarPrimerNombre = By.xpath("//input[@id='txtNombreContacto']");
    private  By SelectAgregarPrimerApellido = By.xpath("//input[@id='txtPrimerApellido']");
    private  By SelectAgregarDireccion = By.xpath("//input[@id='txtDirreccion']");

    private  By inputAgregarCargo = By.xpath("//input[@id='txtCargo']");

    private  By SelectAgregarOpcionTitulo = By.xpath("//div[@id='MainContent_wucCustomDataContact_ddlTratamiento_chosen']");

    private  By selectOpcionTipoTitulo = By.xpath("//div[18]/div/div/div/ul/li[1]");

    private  By SelectAgregarOpcionDepartamento = By.xpath("//div[@id='MainContent_wucCustomDataContact_DropDownListDepartamento_chosen']");
    private  By selectOpcionDepartamento = By.xpath("//div[20]/div/div/div/ul/li[4]");
    private  By SelectagregarCiudad = By.xpath("//div[@id='MainContent_wucCustomDataContact_DropDownListCiudad_chosen']");

    private  By selectOpcionCiudad = By.xpath("//div[21]/div/div/div/ul/li[2]");
    private  By inputAgregarEstrato= By.xpath("//input[@id='MainContent_wucCustomDataContact_rpIndices_txtData_0']");
    private  By SelectagregarGenero = By.xpath("//div[@id='MainContent_wucCustomDataContact_rpIndices_ddlData_1_chosen']");
    private  By selectOpcionGenero= By.xpath("//div[2]/div/div/div/ul/li[2]");


    private By clickGuardar = By.xpath("//a[@id='MainContent_btnGuardarContacto']");




    public By getClickPersonaliza() {return clickPersonaliza;}
    public By getBuscarparticular() {return buscarparticular;}
    public By getAgregarparticular() {return agregarparticular;}

    public By getSelectTipoPersona() {return selectTipoPersona;}
    public By getSelectInputTipoPersona() {return selectInputTipoPersona;}
    public By getSelectTipodeDocumento() {return selectTipodeDocumento;}

    public By getSelectOpcionTipoDocumento() {return selectOpcionTipoDocumento;}

    public By getSelectAgregarPIdentificacion() {
        return SelectAgregarPIdentificacion;
    }

    public By getSelectAgregarPrimerNombre() {
        return SelectAgregarPrimerNombre;
    }

    public By getSelectAgregarPrimerApellido() {
        return SelectAgregarPrimerApellido;
    }

    public By getSelectAgregarDireccion() {
        return SelectAgregarDireccion;
    }

    public By getInputAgregarCargo() {
        return inputAgregarCargo;
    }

    public By getSelectAgregarOpcionTitulo() {
        return SelectAgregarOpcionTitulo;
    }
    public By getSelectOpcionTipoTitulo() {
        return selectOpcionTipoTitulo;
    }


    public By getSelectAgregarOpcionDepartamento() {
        return SelectAgregarOpcionDepartamento;
    }

    public By getSelectOpcionDepartamento() {
        return selectOpcionDepartamento;
    }

    public By getSelectagregarCiudad() {
        return SelectagregarCiudad;
    }

    public By getSelectOpcionCiudad() {
        return selectOpcionCiudad;
    }

    public By getInputAgregarEstrato() {
        return inputAgregarEstrato;
    }

    public By getSelectagregarGenero() {
        return SelectagregarGenero;
    }

    public By getSelectOpcionGenero() {
        return selectOpcionGenero;
    }

    public By getClickGuardar() {
        return clickGuardar;
    }
}

