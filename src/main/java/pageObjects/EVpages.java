package pageObjects;

import org.openqa.selenium.By;

public class EVpages {

    private By Ingresosuariotxt = By.xpath("//input[@id='Login1_UserName']");
    private By ingresarcontrasenatxt = By.xpath("//input[@id='Login1_Password']");
    private  By clickboton = By.xpath("//a[@id='Login1_btnLogIn']");

    public By getIngresosuariotxt() {return Ingresosuariotxt;}
    public By getIngresarcontrasena() {return ingresarcontrasenatxt;}

    public By getClickboton() {return clickboton;}
}
